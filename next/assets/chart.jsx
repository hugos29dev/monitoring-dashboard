import ChartJS from 'chart.js'
import React from 'react'
import pt from 'prop-types'

/**
 * A chart element
 * @render react
 * @name Chart
 */

class Chart extends React.Component {
  constructor(props) {
    super(props)
    this.Canvas = ''
    this.state = {
      chart: null,
    }
  }

  componentDidMount() {
    const {
      type,
      options,
      datasets,
      labels: PpsLabels,
      title,
      legend: PpsLegend,
    } = this.props
    const labels = []

    const num = 6
    for (let i = 0; i < num; i++) {
      labels.push(i.toString())
    }

    let legend = {
      ...options.legend,
      display: PpsLegend,
    }

    this.setState({
      chart: new ChartJS(this.Canvas, {
        type,
        data: {
          labels: PpsLabels || labels,
          datasets: datasets || [],
        },
        options: {
          title: {
            fontSize: 20,
            fontColor: '#FCFCFC',
            display: !!title,
            text: title || undefined,
          },
          ...options,
          legend,
        },
      }),
    })
    this.loadData()
  }

  loadData = async () => {
    const num = 6

    let res

    try {
      res = await fetch(`http://localhost:5000/api/data/${num}`)
    } catch (error) {
      alert(error)
      return alert('API indisponible')
    }

    const FakeData = await res.json()

    const dataset = {
      label: 'Fausses données',
      data: FakeData,
      backgroundColor: [
        'rgba(100,181,246, 0.2)',
        'rgba(229,115,115, 0.2)',
        'rgba(240,98,146, 0.2)',
        'rgba(149,117,205, 0.2)',
        'rgba(38,166,154, 0.2)',
        'rgba(255,202,40, 0.2)',
      ],
      borderColor: [
        'rgba(33,150,243, 1)',
        'rgba(244,67,54, 1)',
        'rgba(233,30,99, 1)',
        'rgba(103,58,183, 1)',
        'rgba(0,137,123, 1)',
        'rgba(255,179,0, 1)',
      ],
      borderWidth: 1,
    }
    this.state.chart.data.datasets[0] = dataset
    this.state.chart.update()
  }

  render() {
    const { height, width } = this.props.size
    return (
      <>
        <canvas ref={c => (this.Canvas = c)} style={{ ...this.props.size }} />
        <style jsx>
          {`
            canvas {
              height: ${height} !important;
              width: ${width} !important;
            }
          `}
        </style>
      </>
    )
  }
}

Chart.propTypes = {
  size: pt.shape({
    width: pt.oneOfType([pt.string, pt.number]),
    height: pt.oneOfType([pt.string, pt.number]),
  }),
  type: pt.oneOf([
    'line',
    'bar',
    'horizontalBar',
    'radar',
    'doughnut',
    'polarArea',
    'bubble',
    'pie',
    'scatter',
  ]).isRequired,
  datasets: pt.oneOfType([pt.arrayOf(pt.object), pt.object]),
  options: pt.object.isRequired,
  labels: pt.arrayOf(pt.string),
  title: pt.string,
  legend: pt.bool,
}

export default Chart
