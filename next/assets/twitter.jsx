import React from 'react'
import { XmlEntities as Decoder } from 'html-entities'

class Twitter extends React.Component {
  state = { Tweets: [] }

  componentDidMount = async () => {
    const RawData = await fetch('http://localhost:5000/api/twitter')
    const Data = await RawData.json()
    const Tweets = Data.statuses
    this.setState({ Tweets })
  }

  styleTD = {
    border: 'node !important',
  }
  /*style={this.styleTD}*/
  verifData = tweet => (
    <tr key={tweet.id}>
      <td>
        <img src={tweet.user.profile_image_url} alt="" />
      </td>
      <td>{Decoder.decode(tweet.text)}</td>
      <style jsx>
        {`
          tr {
            color: white;
            font-family: 'SegoeUI', Tahoma, Geneva, Verdana, sans-serif;
            font-size: 10pt;
          }

          td {
            min-height: 0 !important;
            max-height: none !important;
            width: auto;
            border: none !important;
            height: 40px;
            /*height: auto;*/
          }

          img {
            height: 40px;
            border-radius: 50%;
          }
        `}
      </style>
    </tr>
  )

  render() {
    return (
      <>
        <table>
          <tbody>
            <tr>
              <td colSpan={2}>
                <h2>Twitter</h2>
              </td>
            </tr>
            {this.state.Tweets.map(this.verifData)}
          </tbody>
        </table>
        <style jsx>{`
          h2 {
            height: auto;
          }

          td {
            min-height: 0 !important;
            max-height: none !important;
            width: auto;
            border: none !important;
            height: auto;
          }

          tbody {
            min-height: auto;
            min-width: auto;
          }

          table {
            height: 22vh !important;
            max-height: 22vh !important;
            overflow: hidden;
            min-height: auto;
            min-width: auto;
          }

          * {
            border: none !important;
          }

          tr {
            color: white;
            font-family: 'SegoeUI', Tahoma, Geneva, Verdana, sans-serif;
            font-size: 10pt;
          }

          img {
            border-radius: 50%;
          }
        `}</style>
      </>
    )
  }
}

export default Twitter
