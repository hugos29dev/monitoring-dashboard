import { Map, TileLayer, Marker } from 'react-leaflet'
import MarkerClusterGroup from 'react-leaflet-markercluster'
import L from 'leaflet'
import pt from 'prop-types'
import React from 'react'

//import 'react-leaflet-markercluster/dist/styles.min.css'

class MapAsset extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Map
        center={this.props.center}
        zoom={this.props.zoom}
        maxZoom={18}
        className="markercluster-map"
      >
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <MarkerClusterGroup>
          {this.props.markers.map(props => (
            <Marker {...props} />
          ))}
        </MarkerClusterGroup>
      </Map>
    )
  }
}

MapAsset.propType = {
  markers: pt.arrayOf(
    pt.shape({
      pos: pt.arrayOf(pt.number),
    })
  ).isRequired,
  size: pt.shape({
    height: pt.oneOfType([pt.number, pt.string]),
    width: pt.oneOfType([pt.number, pt.string]),
  }),
  zoom: pt.number.isRequired,
  center: pt.arrayOf(pt.number).isRequired,
}

export default MapAsset
