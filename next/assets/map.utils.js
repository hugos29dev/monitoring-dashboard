import genId from 'uniqid'


export const addId = position => {
  return { key: genId(), position }
}