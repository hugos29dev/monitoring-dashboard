import pt from 'prop-types'

export const Box = ({ children, name }) => (
  <div style={{ gridArea: name }}>{children}</div>
)

Box.propTypes = {
  name: pt.string.isRequired,
}

export const Container = ({ children, areas, cols, rows }) => (
  <main
    style={{
      gridTemplateAreas: areas,
      gridTemplateColumns: cols,
      gridTemplateRows: rows,
      display: 'grid',
    }}
  >
    {children}
  </main>
)

Container.propTypes = {
  areas: pt.string.isRequired,
  cols: pt.string.isRequired,
  rows: pt.string.isRequired,
}

export default { Box, Container }
/**
  <>

 <style jsx>
      {`
        main {
          grid-template-area: ` +
        areas +
        `;
          grid-template-columns: ${cols};
          grid-template-rows: ${rows};
          display: grid;
        }
      `}
    </style>
  </>
 */
