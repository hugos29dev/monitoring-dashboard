import React from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import Chart from './chart'
import toJSON from 'enzyme-to-json'

import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
configure({ adapter: new Adapter() })

const Component = (
  <Chart
    size={{ height: '22vh', width: 3 * 20 + '%' }}
    options={{}}
    type="line"
  />
)

describe('Mounting & unmounting', () => {
  const div = document.createElement('div')
  test('monting', () => {
    render(Component, div)
  })
  test('unmounting', () => {
    unmountComponentAtNode(div)
  })
})

describe('Rendering', () => {
  test('render a canvas', () => {
    expect(shallow(Component).is('canvas')).toBe(true)
  })
  test('render with the same structure', () => {
    expect(toJSON(shallow(Component))).toMatchSnapshot()
  })
})

/**
 * 
 * 
  size?: ChartSize
  type: ChartJS.ChartType
  datasets?: ChartJS.ChartDataSets[]
  options: ChartJS.ChartOptions
  labels?: string[]
  title?: string
  legend?: boolean
 */
