import React from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import Twitter from './twitter'
import toJSON from 'enzyme-to-json'

import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
configure({ adapter: new Adapter() })

const Component = <Twitter />

describe('Mounting & unmounting', () => {
  const div = document.createElement('div')
  test('monting', () => {
    render(Component, div)
  })
  test('unmounting', () => {
    unmountComponentAtNode(div)
  })
})

describe('Rendering', () => {
  test('render with the same structure', () => {
    expect(toJSON(shallow(Component))).toMatchSnapshot()
  })
})

/**
 * 
 * 
  size?: ChartSize
  type: ChartJS.ChartType
  datasets?: ChartJS.ChartDataSets[]
  options: ChartJS.ChartOptions
  labels?: string[]
  title?: string
  legend?: boolean
 */
