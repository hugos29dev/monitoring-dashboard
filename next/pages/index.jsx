import React from 'react'

import Page1 from './1'
import Page2 from './2'

export default class SwitchPage extends React.Component {
  state = { currentPage: 1, LastPage: 2, intID: -999 }

  constructor(props) {
    super(props)
    const intID = setInterval(
      () =>
        this.setState({
          currentPage:
            this.state.currentPage + 1 >= this.state.LastPage + 1
              ? 1
              : this.state.currentPage + 1,
        }),
      3000
    )
    const { state } = this
    this.state = { intID, ...state }
  }

  componentWillUnmount() {
    clearInterval(this.state.intID)
  }

  render() {
    switch (this.state.currentPage) {
      case 1:
        return <Page1 />
      case 2:
        return <Page2 />
      default:
        break
    }
  }
}
