import React from 'react'

import { Box, Container } from '../assets/layout'
import Chart from './../assets/chart'
import Map from '../assets/map.noSSR'
import { addId } from '../assets/map.utils'

class Page2 extends React.Component {
  state = {
    data: [0, 2, 3, 8, 2, 6],
  }
  render() {
    //const val = [50, -4, 40, 9]
    const { random } = Math
    const genMarkers = length => {
      const arr = []
      for (let i = 0; i < length; i++) {
        const lat = random() * 10 + 40
        const lng = random() * 13 - 4
        arr.push([lat, lng])
      }
      return arr
    }

    const markers = genMarkers(random() * 10000)

    const areas = `
      "ti ti ti ti ti"
      "c1 c1 c4 c2 c2"
      "c1 c1 c3 c3 c3"
      "c5 c5 c3 c3 c3"
      "c6 c6 c3 c3 c3"
    `
    const rows = '80px 1fr 1fr 1fr 1fr'
    const cols = '1fr 1fr 1fr 1fr 1fr'

    const fontSize = 20

    const defaultColors = {
      backgroundColor: [
        'rgba(100,181,246, 0.2)',
        'rgba(229,115,115, 0.2)',
        'rgba(240,98,146, 0.2)',
        'rgba(149,117,205, 0.2)',
        'rgba(38,166,154, 0.2)',
        'rgba(255,202,40, 0.2)',
      ],
      borderColor: [
        'rgba(33,150,243, 1)',
        'rgba(244,67,54, 1)',
        'rgba(233,30,99, 1)',
        'rgba(103,58,183, 1)',
        'rgba(0,137,123, 1)',
        'rgba(255,179,0, 1)',
      ],
      borderWidth: 1,
    }

    const options = {
      animation: {},
      scales: {
        yAxes: [
          {
            ticks: {
              fontColor: '#E0E0E0',
              fontSize,
              lineHeight: 2,
            },
          },
        ],
      },
      legend: {
        labels: {
          fontSize,
          fontColor: '#BDBDBD',
        },
      },
    }

    return (
      <Container {...{ areas, cols, rows }}>
        <Box name="ti">
          <h1>Dashboard Zenviron 2</h1>
        </Box>
        <Box name="c1">
          <Chart
            size={{
              height: 'calc((100vh - 80px) / 4 * 2)',
              width: 'calc(100vw / 5 * 2)',
            }}
            options={options}
            datasets={[
              {
                label: 'test',
                data: this.state.data,
                ...defaultColors,
              },
            ]}
            type="bar"
          />
        </Box>
        <Box name="c2">
          <Chart
            size={{
              height: 'calc((100vh - 80px) / 4 * 1)',
              width: 'calc(100vw / 5 * 2)',
            }}
            options={options}
            type="line"
          />
        </Box>
        <Box name="c3">
          <Map
            zoom={6}
            center={[46.301366, 2.781833]}
            size={{
              height: 'calc((100vh - 80px) / 4 * 3)',
              width: 'calc(100vw / 5 * 3)',
            }}
            markers={markers.map(m => addId(m))}
          />
        </Box>
        <Box name="c4">
          <Chart
            size={{
              height: 'calc((100vh - 80px) / 4 * 1)',
              width: 'calc(100vw / 5 * 1)',
            }}
            options={options}
            type="horizontalBar"
          />
        </Box>
        <Box name="c5">
          <Chart
            size={{
              height: 'calc((100vh - 80px) / 4 * 1)',
              width: 'calc(100vw / 5 * 2)',
            }}
            options={options}
            type="line"
          />
        </Box>
        <Box name="c6">
          <Chart
            size={{
              height: 'calc((100vh - 80px) / 4 * 1)',
              width: 'calc(100vw / 5 * 2)',
            }}
            options={options}
            type="bar"
          />
        </Box>
      </Container>
    )
  }
}

export default Page2
