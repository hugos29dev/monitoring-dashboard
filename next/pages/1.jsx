import React from 'react'

import { Box, Container } from '../assets/layout'
import Chart from '../assets/chart.noSSR'
import Twitter from '../assets/twitter'

class Page1 extends React.Component {
  state = {
    data: (num, incr) => {
      const nb = num || 6
      const increment = incr || false
      const { random, floor } = Math
      const data = []
      let prev = 0
      for (let i = 0; i < nb; i++) {
        const rand = floor(random() * 255)
        if (!increment) {
          data.push(rand)
        } else {
          data.push(prev)
          prev += floor(rand ** 2 / 200)
        }
      }

      return data
    },
  }
  render() {
    const areas = `
      "ti ti ti ti ti"
      "c1 c1 c1 c2 c2"
      "c3 c3 c4 c4 c4"
      "c3 c3 c7 c6 c6"
      "c5 c5 c7 c6 c6"
    `
    const rows = '80px 1fr 1fr 1fr 1fr'
    const cols = '1fr 1fr 1fr 1fr 1fr'

    const fontSize = 14

    const defaultColors = {
      backgroundColor: [
        'rgba(100,181,246, 0.2)',
        'rgba(229,115,115, 0.2)',
        'rgba(240,98,146, 0.2)',
        'rgba(149,117,205, 0.2)',
        'rgba(38,166,154, 0.2)',
        'rgba(255,202,40, 0.2)',
        'rgba(100,181,246, 0.2)',
        'rgba(229,115,115, 0.2)',
        'rgba(240,98,146, 0.2)',
        'rgba(149,117,205, 0.2)',
        'rgba(38,166,154, 0.2)',
        'rgba(255,202,40, 0.2)',
      ],
      borderColor: [
        'rgba(33,150,243, 1)',
        'rgba(244,67,54, 1)',
        'rgba(233,30,99, 1)',
        'rgba(103,58,183, 1)',
        'rgba(0,137,123, 1)',
        'rgba(255,179,0, 1)',
        'rgba(33,150,243, 1)',
        'rgba(244,67,54, 1)',
        'rgba(233,30,99, 1)',
        'rgba(103,58,183, 1)',
        'rgba(0,137,123, 1)',
        'rgba(255,179,0, 1)',
      ],
      borderWidth: 1,
    }

    const options = {
      animation: {},
      scales: {
        xAxes: [
          {
            ticks: {
              fontColor: '#E0E0E0',
              fontSize,
              lineHeight: 2,
            },
          },
        ],
        yAxes: [
          {
            ticks: {
              fontColor: '#E0E0E0',
              fontSize,
              lineHeight: 2,
            },
          },
        ],
      },
      legend: {
        display: false,
        labels: {
          fontSize,
          fontColor: '#BDBDBD',
        },
      },
    }

    return (
      <Container {...{ areas, cols, rows }}>
        <Box name="ti">
          <h1>Dashboard Zenviron</h1>
        </Box>
        <Box name="c1">
          <Chart
            size={{
              height: 'calc((100vh - 80px) / 4 * 1)',
              width: 'calc(100vw / 5 * 3)',
            }}
            {...{ options }}
            title="Nombre de visites par villes"
            labels={[
              'Plabennec',
              'Brest',
              'Plouguerneau',
              'Saint Renan',
              'Bohars',
              'Lesneven',
              'Landerneau',
              'Autre (29)',
              'Autre (FR)',
            ]}
            datasets={[
              {
                label: 'Visites',
                data: this.state.data(10),
                ...defaultColors,
              },
            ]}
            type="bar"
          />
        </Box>
        <Box name="c2">
          <Chart
            size={{
              height: 'calc((100vh - 80px) / 4 * 1)',
              width: 'calc(100vw / 5 * 2)',
            }}
            title="Temps de latence par OS par nombre de clients connéctés"
            labels={['0-10', '10-50', '50-100', '100-500', '500-999', '+999']}
            legend
            options={{
              ...options,
              scales: {
                yAxes: [
                  {
                    ticks: {
                      callback: (v, i, d) => v + 100 + 'ms',
                      ...(options.scales &&
                        options.scales.yAxes &&
                        options.scales.yAxes[0].ticks),
                    },
                  },
                ],
              },
            }}
            type="line"
            datasets={[
              {
                label: 'IOS',
                data: this.state.data(6, true),
                ...defaultColors,
              },
              {
                label: 'Android',
                data: this.state.data(6, true),
                backgroundColor: [
                  'rgba(255,202,40, 0.2)',
                  ...defaultColors.backgroundColor,
                ],
                borderColor: [
                  'rgba(255,179,0, 1)',
                  ...defaultColors.backgroundColor,
                ],
                borderWidth: defaultColors.borderWidth,
              },
            ]}
          />
        </Box>
        <Box name="c3">
          <Twitter />
        </Box>
        <Box name="c4">
          <Chart
            size={{
              height: 'calc((100vh - 80px) / 4 * 1)',
              width: 'calc(100vw / 5 * 3)',
            }}
            {...{ options }}
            title="Nombre d'ouverture(s) de l'application par heure"
            type="horizontalBar"
            labels={[
              '0h - 4h',
              '4h - 8h',
              '8h - 12h',
              '12h - 16h',
              '16h - 20h',
              '20h - 24h',
            ]}
            datasets={[
              {
                label: "Ouverture(s) de l'application",
                data: this.state.data(6),
                ...defaultColors,
              },
            ]}
          />
        </Box>
        <Box name="c5">
          <Chart
            size={{
              height: 'calc((100vh - 80px) / 4 * 1)',
              width: 'calc(100vw / 5 * 2)',
            }}
            {...{ options }}
            type="line"
            labels={['2017', '2018', '2019']}
            title="Nombre d'installation depuis 2017"
            datasets={[
              {
                label: 'Installations',
                data: this.state.data(3, true),
                ...defaultColors,
              },
            ]}
          />
        </Box>
        <Box name="c6">
          <Chart
            size={{
              height: 'calc((100vh - 80px) / 4 * 2)',
              width: 'calc(100vw / 5 * 2)',
            }}
            {...{ options }}
            type="radar"
          />
        </Box>
        <Box name="c7">
          <Chart
            size={{
              height: 'calc((100vh - 80px) / 4 * 2)',
              width: 'calc(100vw / 5 * 1)',
            }}
            {...{ options }}
            datasets={[
              {
                label: 'Ajouts',
                data: this.state.data(12),
                ...defaultColors,
              },
            ]}
            labels={[
              'Jan.',
              'Fev.',
              'Mar.',
              'Avr.',
              'Mai.',
              'Jui.',
              'Jui.',
              'Aoû.',
              'Sep.',
              'Oct.',
              'Nov.',
              'Déc.',
            ]}
            title="Ajout de repères par mois"
            type="bar"
          />
        </Box>
      </Container>
    )
  }
}

export default Page1
