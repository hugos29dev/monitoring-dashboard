import App, { Container } from 'next/app'
import Head from 'next/head'
import React from 'react'

export default class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }

  render() {
    const { Component, pageProps } = this.props

    return (
      <Container>
        <Head>
          <title>Monitoring</title>
          <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/leaflet/dist/leaflet.css"
          />
          <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/react-leaflet-markercluster/dist/styles.min.css"
          />
          <link rel="stylesheet" href="static/global.css" />
        </Head>
        <Component {...pageProps} />
      </Container>
    )
  }
}
