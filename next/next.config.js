const env = require('next-env')
const dotenvLoad = require('dotenv-load')
const css = require('@zeit/next-css')

const applyNext = (arr, data) => {
  let prev = data
  arr.forEach(sup => (prev = sup(prev)))
  return prev
}

dotenvLoad()

module.exports = applyNext([env, css], {})
