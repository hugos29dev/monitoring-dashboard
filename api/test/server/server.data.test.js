import SuperTest from 'supertest'
import app from './../../src/app'

const req = SuperTest(app)

describe('Create data', () => {
  test('Return a status of 200 when sending a number', () =>
    req.get('/api/data/6').then(res => expect(res.statusCode).toBe(200)))
  test('Return a status of 400 when not sending a number', () =>
    req
      .get('/api/data/uaoilruze')
      .then(res => expect(res.statusCode).toBe(400)))
  test('Return an array of number | 0 <=> 255', () =>
    req.get('/api/data/50').then(res => {
      const { body } = res
      expect(Array.isArray(body)).toBe(true)
      body.forEach(d => {
        expect(typeof d).toBe('number')
        expect(d).toBeLessThanOrEqual(255)
        expect(d).toBeGreaterThanOrEqual(0)
      })
    }))
})
