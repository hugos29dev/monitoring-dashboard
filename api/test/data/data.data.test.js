import GenData from './../../src/routes/api/data/data'

describe('Create fake data', () => {
  describe('# Return an array', () => {
    test('only', () => expect(Array.isArray(GenData(6))).toBe(true))
    test('of numbers', () =>
      GenData(10).forEach(d => expect(typeof d).toBe('number')))
  })
  describe('# Return the number of elements we need', () => {
    test('0 elements', () => expect(GenData(0).length).toBe(0))
    test('0 elements', () => expect(GenData(0).length).toBe(0))
    test('4 elements', () => expect(GenData(4).length).toBe(4))
    test('6 elements', () => expect(GenData(6).length).toBe(6))
    test('8 elements', () => expect(GenData(8).length).toBe(8))
    test('10 elements', () => expect(GenData(10).length).toBe(10))
    test('12 elements', () => expect(GenData(12).length).toBe(12))
  })
  describe('# Return only number 0 <=> 255', () => {
    const num = 10
    for (let i = 0; i < num; i++) {
      const data = GenData(10)
      test(`test[${i}]: ${data} | 0 <=> 255`, () => {
        for (const d of data) {
          expect(d).toBeLessThanOrEqual(255)
          expect(d).toBeGreaterThanOrEqual(0)
        }
      })
    }
  })
})
