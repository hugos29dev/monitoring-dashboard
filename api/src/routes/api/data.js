import express from 'express'

import GenData from './data/data'

const router = express()

router.get('/:size', (req, res) => {
  try {
    const size = parseInt(req.params.size)

    if (isNaN(size) || size == undefined || size == null)
      return res.status(400).send({
        message: "the size isn't a number",
      })

    const data = GenData(size)

    res.json(data)
  } catch (e) {
    return res.status(400).send(e)
  }
})

export default router
