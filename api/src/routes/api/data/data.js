export default size => {
  const data = []

  const { floor, random } = Math

  for (let i = 0; i < size; i++) {
    data.push(floor(random() * 255))
  }
  return data
}
