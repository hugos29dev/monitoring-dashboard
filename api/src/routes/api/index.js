import express from 'express'

import DataRoute from './data'
import TwitterRoute from './twitter'

const router = express()

router.use('/data', DataRoute)
router.use('/twitter', TwitterRoute)

export default router
