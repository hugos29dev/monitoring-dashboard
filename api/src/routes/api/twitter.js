import express from 'express'
import { config } from 'dotenv'

config()
const router = express()

import { Base64 } from 'js-base64'
import URLEncoder from 'urlencode'
import fetch from 'node-fetch'

let AuthToken
let tweets = {}
const { TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET } = process.env

function VerifParams() {
  const params = [TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET]
  for (const p of params) {
    if (p == undefined)
      return console.log(`
        Some params of Twitter's config aren't defined,
        A .env file may not be created ?
      `)
  }
}

async function getToken() {
  VerifParams()

  const auth = Base64.encode(
    `${URLEncoder.encode(TWITTER_CONSUMER_KEY)}:${URLEncoder.encode(
      TWITTER_CONSUMER_SECRET
    )}`
  )

  const rawData = await fetch('https://api.twitter.com/oauth2/token', {
    method: 'post',
    headers: {
      Authorization: `Basic ${auth}`,
      'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
    body: 'grant_type=client_credentials',
  })
  const data = await rawData.json()
  return (AuthToken = data.access_token)
}

async function getTweets() {
  const data = await fetch(
    `https://api.twitter.com/1.1/search/tweets.json?q=${URLEncoder.encode(
      process.env.TWITTER_QUERY
    )}&result_type=mixed&count=8`,
    {
      headers: {
        Authorization: `Bearer ${AuthToken}`,
      },
    }
  )
  return (tweets = await data.json())
}

getToken().then(getTweets)

const IntID = setInterval(getTweets, 60000)

router.get('/', (req, res) => res.json(tweets))

export default router
