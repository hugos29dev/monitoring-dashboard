import express from 'express'
import morgan from 'morgan'
import app from './app'
import { config } from 'dotenv'
config()
const { PORT } = process.env

app.use(morgan('dev'))

const port = PORT || 5000
app.listen(port, () => console.log(`Listenning to port ${port}...`))
