import express, { Request, Response } from 'express'
import cors from 'cors'
import { config } from 'dotenv'

import listRoutes from 'express-list-endpoints'
import ApiRoutes from './routes/api/index'

config()
const {} = process.env

const app = express()

app.use(cors())
app.use('/api', ApiRoutes)

export default app
