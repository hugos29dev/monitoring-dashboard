# Librairies

| Fonctions   | Librairie(s)            |
| ----------- | ----------------------- |
| Graphiques  | Chart.js                |
| Framework   | React & React Dom       |
| Styles      | CSS & Styled Components |
| Routes HTTP | React Router (Dom)      |
